$(document).ready(function(){
    var mergedList = MergeModels({
		models:[Gen1, Gen2, Gen3, Gen4, Gen5, Gen6, Gen7, Gen8, Variant, Mine, Alternate] //Add the variable for the model here, or remove to take a file out of the merge.
	});

	RandomMenu({
		title: "Random Party",
		sort: 
		{
			keys: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
		}
	});

	$("#pokedex").css("visibility", "hidden");

	$("#go_button").click(function(){
		var selected = $("#sort").val();
		//console.log(selected);
		generateParty(mergedList, selected);	
	});
});

function generateParty(list, party_size){
	var shuffled = [];

	for(key in list){
		shuffled.push(key);
	}
	shuffled = shuffle(shuffled);
	var party = shuffled.slice(0,party_size);

	console.log(party);

	displayPokemon(list, party);

	//return party;
}

// This function shuffles the data and returns the result
function shuffle(o) {
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
}

function displayPokemon(allPkmn, party)
{
	$.each(party, function(pokemonName, pokemon) {
		var pkmn = findPokemon(allPkmn, pokemon);
		// console.log(pkmn);
		if ( pkmn.Types.length == 1 ) { pkmn.Types[1] = pkmn.Types[0]; }
		else if ( pkmn.Types.length > 2 ) { return; }

		var article = $("<article class='pokemon three columns border "+ pkmn.Types[0].toLowerCase() +"border' id='"+pokemon+"' title=''></article>");
		if(!pokemon.includes("Nidoran")){
			article.append(
				"<div class='flex twelve columns'>"+
					"<img class='icon one column' src= 'assets/images/type_icons/"+pkmn.Types[0]+".png' alt='"+pkmn.Types[0]+"'/>"+
					"<span class='ten columns'>"+
						"<h5 class='header Name'><span>"+pokemon.replace(/_/g, " ")+" : </span>"+
							"<span class='Pokedex_ID'>"+pkmn.Pokedex_ID+"</span></h5>"+
						"</span>"+
					"<img class='icon one column Type' src= 'assets/images/type_icons/"+pkmn.Types[1]+".png' alt='"+pkmn.Types[1]+"'/>"+
				"</div>");
		}
		if(pokemon.includes('(m)')){
			article.append(
			"<div class='flex twelve columns'>"+
				"<img class='icon one column' src= 'assets/images/type_icons/"+pkmn.Types[0]+".png' alt='"+pkmn.Types[0]+"'/>"+
				"<span class='ten columns'>"+
					"<h5 class='header Name'><span>"+pokemon.replace("(m)", " \♂")+" : </span>"+
	    			"<span class='Pokedex_ID'>"+pkmn.Pokedex_ID+"</span></h5>"+
	    		"</span>"+
				"<img class='icon one column Type' src= 'assets/images/type_icons/"+pkmn.Types[1]+".png' alt='"+pkmn.Types[1]+"'/>"+
			"</div>");
		}
		if(pokemon.includes('(f)')){
			article.append(
			"<div class='flex twelve columns'>"+
				"<img class='icon one column' src= 'assets/images/type_icons/"+pkmn.Types[0]+".png' alt='"+pkmn.Types[0]+"'/>"+
				"<span class='ten columns'>"+
					"<h5 class='header Name'><span>"+pokemon.replace("(f)", " \♀")+" : </span>"+
	    			"<span class='Pokedex_ID'>"+pkmn.Pokedex_ID+"</span></h5>"+
	    		"</span>"+
				"<img class='icon one column Type' src= 'assets/images/type_icons/"+pkmn.Types[1]+".png' alt='"+pkmn.Types[1]+"'/>"+
			"</div>");
		}
		if(pokemon !== "Farfetch\'d"){
			article.append("<div class='twelve columns'><img class='pokepic offset-by-one ten columns' src='assets/images/pokemon/"+pokemon+".png' /></div>");
		}
		if(pokemon === "Farfetch\'d"){
			article.append("<div class='twelve columns'><img class='pokepic offset-by-one ten columns' src='assets/images/pokemon/Farfetch.png' /></div>");
		}
		article.append("<div class='center twelve columns'><span>"+pkmn.Short_Description+"</span></div>");

		$("#pokedex").append(article);

		article.tooltip({
			content: "<div><p>"+ pkmn.Long_Description +"</p></div>",
			open: function(event, ui)
	        {
	        	ui.tooltip.hover(
	            function () {
	            	$(this).fadeTo("slow", 1);
				});
	        },
			track: true
		});

		$("#pokedex").css("visibility", "visible");

		article.on('click', function() {
			window.open("pokemon.html?name="+pokemon + "&mode="+ $("input[name=modeSelect]:checked").val(), '_blank');
		});
	});
}

function findPokemon(all, pkmnName) 
  {
    var move = null;
    $.each(all, function(pokeName, pkmnList) {
	  if (pokeName === pkmnName)
      {	
        move = pkmnList;
		console.log(move);
      } 
    });
    return move;
  }