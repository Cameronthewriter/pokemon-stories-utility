/*Mysterious code goes here*/

$( document ).ready( function() {

	//This is used to merge json objects from other included files
	var mergedList = MergeModels({
		models:[Gen1, Gen2, Gen3, Gen4, Gen5, Gen6, Gen7, Gen8, Variant, Mine, Alternate]
	});

	console.log(mergedList);

	//instantiate menu with search and sort
	Menu({
		model: mergedList,
		title: "Pokedex",
		search: 
		{ 
			tags: function() 
	        {
	            var tags = [];
	            $.each(mergedList, function(name) {
	               tags.push(name.replace(/_/g, " "));
	            });
	        	return tags;
	        },
			funct: search 
		},
		sort: 
		{
			keys: ['Pokedex_ID', 'Name', 'Type'],
			funct: sort
		}
	});
	
	//add the radio buttons for Pokemon generation
	$("#selector").append(
        '<div id="radioGroup" class="right">' + 
          '<input type="radio" name="modeSelect" value="Starter">Starter</input>' +
          '<input type="radio" name="modeSelect" value="Weak">Weak</input>' +
          '<input type="radio" name="modeSelect" value="Average">Average</input>' +
          '<input type="radio" name="modeSelect" value="Strong">Strong</input>' +
          '<input type="radio" name="modeSelect" value="Maxed">Maxed</input>' +
        '</div>');
	
	displayPokemon(mergedList);
	sort();
});	

function search()
{ 
    var input = $("#search").val().toLowerCase().replace(/ /g, '_');
    var items = $(".pokemon");
    $.each(items, function() { 
        var name = $(this).find(".Name").text().replace(/ /g, '_');
        if (name.toLowerCase().indexOf(input)!=-1) {
          $(this).show(); //should be a table row
        }
        else { 
          $(this).hide(); //should be a table row
        }
    });
}

//Sort options types, name, pokedex number
function sort()
{
	var selected = $("#sort").val();
	var items = $(".pokemon");
	items = items.sort(function (a, b) {
	  if ( selected === "Pokedex_ID" ) { 
	    return $(a).find("."+selected).text() > $(b).find("."+selected).text() ? 1 : -1; 
	  }
	  else if ( selected === "Name" ) { 
	    return $(a).find("."+selected).text() > $(b).find("."+selected).text() ? 1 : -1; 
	  }
	  else if ( selected === "Type" ) {
	  	return $(a).find(".icon").attr("alt") > $(b).find(".icon").attr("alt") ? 1 : -1;
	  }
	});
	items.detach().appendTo($("#pokedex"));
}

function displayPokemon(pokemonList)
{
	$.each(pokemonList, function(pokemonName, pokemon) {
		if ( pokemon.Types.length == 1 ) { pokemon.Types[1] = pokemon.Types[0]; }
		else if ( pokemon.Types.length > 2 ) { return; }

		var article = $("<article class='pokemon three columns border "+ pokemon.Types[0].toLowerCase() +"border' id='"+pokemonName+"' title=''></article>");
		if(!pokemonName.includes("Nidoran")){
			article.append(
				"<div class='flex twelve columns'>"+
					"<img class='icon one column' src= 'assets/images/type_icons/"+pokemon.Types[0]+".png' alt='"+pokemon.Types[0]+"'/>"+
					"<span class='ten columns'>"+
						"<h5 class='header Name'><span>"+pokemonName.replace(/_/g, " ")+" : </span>"+
							"<span class='Pokedex_ID'>"+pokemon.Pokedex_ID+"</span></h5>"+
						"</span>"+
					"<img class='icon one column Type' src= 'assets/images/type_icons/"+pokemon.Types[1]+".png' alt='"+pokemon.Types[1]+"'/>"+
				"</div>");
		}
		if(pokemonName.includes('(m)')){
			article.append(
			"<div class='flex twelve columns'>"+
				"<img class='icon one column' src= 'assets/images/type_icons/"+pokemon.Types[0]+".png' alt='"+pokemon.Types[0]+"'/>"+
				"<span class='ten columns'>"+
					"<h5 class='header Name'><span>"+pokemonName.replace("(m)", " \♂")+" : </span>"+
	    			"<span class='Pokedex_ID'>"+pokemon.Pokedex_ID+"</span></h5>"+
	    		"</span>"+
				"<img class='icon one column Type' src= 'assets/images/type_icons/"+pokemon.Types[1]+".png' alt='"+pokemon.Types[1]+"'/>"+
			"</div>");
		}
		if(pokemonName.includes('(f)')){
			article.append(
			"<div class='flex twelve columns'>"+
				"<img class='icon one column' src= 'assets/images/type_icons/"+pokemon.Types[0]+".png' alt='"+pokemon.Types[0]+"'/>"+
				"<span class='ten columns'>"+
					"<h5 class='header Name'><span>"+pokemonName.replace("(f)", " \♀")+" : </span>"+
	    			"<span class='Pokedex_ID'>"+pokemon.Pokedex_ID+"</span></h5>"+
	    		"</span>"+
				"<img class='icon one column Type' src= 'assets/images/type_icons/"+pokemon.Types[1]+".png' alt='"+pokemon.Types[1]+"'/>"+
			"</div>");
		}
		if(pokemonName !== "Farfetch\'d"){
			article.append("<div class='twelve columns'><img class='pokepic offset-by-one ten columns' src='assets/images/pokemon/"+pokemonName+".png' /></div>");
		}
		if(pokemonName === "Farfetch\'d"){
			article.append("<div class='twelve columns'><img class='pokepic offset-by-one ten columns' src='assets/images/pokemon/Farfetch.png' /></div>");
		}
		article.append("<div class='center twelve columns'><span>"+pokemon.Short_Description+"</span></div>");

		$("#pokedex").append(article);

		article.tooltip({
			content: "<div><p>"+ pokemon.Long_Description +"</p></div>",
			open: function(event, ui)
	        {
	        	ui.tooltip.hover(
	            function () {
	            	$(this).fadeTo("slow", 1);
				});
	        },
			track: true
		});

		article.on('click', function() {
			window.open("pokemon.html?name="+pokemonName + "&mode="+ $("input[name=modeSelect]:checked").val(), '_blank');
		});
	});
}