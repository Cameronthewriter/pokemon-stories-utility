function RandomMenu(parameters) 
{
  var title = typeof parameters.title == "undefined" ? "Menu" : parameters.title;

  //construct menu items
  $("#menu").append(
      '<div id="anchor"></div>'+
      '<div id="selector" class="block-header twelve columns">'+
        '<div id="title" class="twleve columns center"><h4 class="header">'+title+'</h4></div><br/><br/>'+
      '</div>');

//   if ( sortFlag )
//   {
    $("#selector").append(
      '<div id="sortBar" class="three columns">'+
        '<h5><label class="header offset-by-two four columns" for="sort">Party Size: </label>'+
        '<select class="input right offset-by-one five columns" id="sort"></select></h5>'+
      '</div>');

    //is there a better way to do this?!
    $.each(parameters.sort.keys, function (key, value) {
      $("#sort").append(
        $("<option></option>").val(value).html(value)
      );
    });

    //$("#sort").on("change", function() { parameters.sort.funct(); });

    //add the radio buttons for Pokemon generation
	$("#selector").append(
          '<div id="radioGroup" class="offset-by-two three columns">' + 
          '<input type="radio" name="modeSelect" value="Starter" checked="true">Starter</input>' +
          '<input type="radio" name="modeSelect" value="Weak">Weak</input>' +
          '<input type="radio" name="modeSelect" value="Average">Average</input>' +
          '<input type="radio" name="modeSelect" value="Strong">Strong</input>' +
          '<input type="radio" name="modeSelect" value="Maxed">Maxed</input>' +
        '</div>');
        $("#selector").append('<div id="button_container" class="offset-by-two two columns">' +
        '<input type="button" id="go_button" class="btn" value="Generate Party"/>');

  //$(document).on('click','.btn', function() { parameters.sort.funct(); });
//   } else {
//     $("#searchBar").addClass("offset-by-three");
//   }

//   if ( searchFlag ) 
//   {
//     //Add search bar in
//     $("#selector").append(
//         '<div id="searchBar" class="three columns">'+
//           '<h5><label class="header offset-by-two four columns" for="search">Search</label>'+
//           '<input class="input right offset-by-one five columns" id="search" type="search"></input></h5>'+
//         '</div>');

//     /*Search Functionality*/
//     //Create auto complete feature for search bar
//     $("#search").autocomplete({
//       source: parameters.search.tags(),
//       minLength: Math.max(Object.keys(model).length/50, 2),
//       select: parameters.search.funct(),
//       delay: 500
//     });
//     //bind search functionality to the search box
//     $("#search")
//       .on("keyup", function() { parameters.search.funct(); })
//       .on('autocompleteclose', function () { parameters.search.funct(); });
//   }

  /*Fixed elements*/
  $(function() {
    fixedScroller($("#anchor"), $("#selector"))
  });

  function fixedScroller(anchor, fixedElement) {
    var move = function() {
        var st = $(window).scrollTop();
        var ot = anchor.offset().top;
        if(st > ot) {
            $(fixedElement).addClass("fixed");
        } else {
            if(st <= ot) {
                $(fixedElement).removeClass("fixed");
            }
        }
    };
    $(window).scroll(move);
    move();
  }
}