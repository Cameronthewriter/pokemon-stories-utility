var shiny, legend, ivStrength, evStrength, ivMind, primaryType, secondaryType, 
    ability, evolutionMethod, male, female, other;


$( document ).ready( function() {
  console.log(moves);
  console.log(location.href);

  //Merges the models from all included json files into one unified model.
  var mergedList = MergeModels({
		models:[Gen1, Gen2, Gen3, Gen4, Gen5, Gen6, Gen7, Gen8, Variant, MyPokemon, AlternatePkmn] //Add the variable for the model here, or remove to take a file out of the merge.
	});

  //Extracting key name from url
  var pokemonName = location.search.substring(location.search.indexOf('=')+1, (location.search.indexOf('&')));
  if(/%27/.test(location.href)){
    pokemonName = "Farfetch\'d";
  }
  console.log(pokemonName);
  var mode = location.search.substring((location.search.indexOf('&') + 5)) ? location.search.substring((location.search.indexOf("mode=") + 5)) : null;
  var selectedPokemon = mergedList[pokemonName];
  
  //Load in all variables
  shiny = typeof selectedPokemon.Shiny !== "undefined" && isShiny(selectedPokemon.Shiny) ? "Shiny" : "";
  legend = typeof selectedPokemon.Legendary !== "undefined" && selectedPokemon.Legendary ? "Legend" : "";
  
  ivStrength = typeof selectedPokemon.Attributes.IV !== "undefined" && typeof selectedPokemon.Attributes.IV.Strength !== "undefined" ? selectedPokemon.Attributes.IV.Strength : 0;
  ivSpeed = typeof selectedPokemon.Attributes.IV !== "undefined" && typeof selectedPokemon.Attributes.IV.Speed !== "undefined" ? selectedPokemon.Attributes.IV.Speed : 0;
  ivSense = typeof selectedPokemon.Attributes.IV !== "undefined" && typeof selectedPokemon.Attributes.IV.Sense !== "undefined" ? selectedPokemon.Attributes.IV.Sense : 0;
  ivMind = typeof selectedPokemon.Attributes.IV !== "undefined" && typeof selectedPokemon.Attributes.IV.Mind !== "undefined" ? selectedPokemon.Attributes.IV.Mind : 0;
  evSpeed = typeof selectedPokemon.Attributes.EV !== "undefined" && typeof selectedPokemon.Attributes.EV.Speed !== "undefined" ? selectedPokemon.Attributes.EV.Strength : generateEV();
  evStrength = typeof selectedPokemon.Attributes.EV !== "undefined" && typeof selectedPokemon.Attributes.EV.Strength !== "undefined" ? selectedPokemon.Attributes.EV.Strength : generateEV();
  evSense = typeof selectedPokemon.Attributes.EV !== "undefined" && typeof selectedPokemon.Attributes.EV.Sense !== "undefined" ? selectedPokemon.Attributes.EV.Strength : generateEV();
  evMind = typeof selectedPokemon.Attributes.EV !== "undefined" && typeof selectedPokemon.Attributes.EV.Mind !== "undefined" ? selectedPokemon.Attributes.EV.Mind : generateEV();

  // add move levels
  var levels = setDifficulty(moves, selectedPokemon);
  //console.log(moves);

  primaryType = selectedPokemon.Types !== "undefined" && selectedPokemon.Types.length > 0 ? selectedPokemon.Types[0] : "None";
  secondaryType = selectedPokemon.Types !== "undefined" && selectedPokemon.Types.length == 2 ? selectedPokemon.Types[1] : "None";

  ability = typeof selectedPokemon.Ability !== "undefined" ? abilities[selectedPokemon.Ability]: "None";
  evolutionMethod = typeof selectedPokemon.Evolution !== "undefined" && typeof selectedPokemon.Evolution.Method !== "undefined" && selectedPokemon.Evolution.Method != null ? selectedPokemon.Evolution.Method : "Mysterious";
  nextEvolution = typeof selectedPokemon.Evolution !== "undefined" && typeof selectedPokemon.Evolution.Next_Evolutions !== "undefined" && selectedPokemon.Evolution.Next_Evolutions !== null ? selectedPokemon.Evolution.Next_Evolutions : "None";
  
  male = selectedPokemon.Gender !== "undefined" && selectedPokemon.Gender.Male_Weight !== "undefined" ? selectedPokemon.Gender.Male_Weight : 50;
  female = selectedPokemon.Gender !== "undefined" && selectedPokemon.Gender.Female_Weight !== "undefined" ? selectedPokemon.Gender.Female_Weight : 50;
  other = selectedPokemon.Gender !== "undefined" && selectedPokemon.Gender.Other_Weight !== "undefined" ? selectedPokemon.Gender.Other_Weight : 0;


  displayLabels(selectedPokemon, pokemonName);
  displayHP(selectedPokemon);
  displayFP(selectedPokemon);
  displayAttributes(selectedPokemon);
  displayPokemon(selectedPokemon, pokemonName);
  displayCounters(selectedPokemon, counters);
  displayDashRange(selectedPokemon, abilities);
  displayAbility(selectedPokemon, abilities);
  displayEvolution(selectedPokemon);
  displayNature(selectedPokemon, natures);
  displayMoves(selectedPokemon, moves);

  function isShiny(shinyKey){
    if(!shinyKey){
      var shinyValue = Math.floor((Math.random() * 100) + 1);
      console.log(shinyValue);
      if(shinyValue === 3 || shinyValue === 73 || shinyValue === 100){
        return true;
      }
      return false;
    }
    return true;
  }
  
  /**
   * The following function is used to generate the EV stats
   */
  function generateEV(){
    if(mode === "Starter"){
      return 1;
    }
    
    if(mode === "Weak"){
      return Math.floor(Math.random() * (4 - 1 + 1)) + 1;
    }
    
    if(mode === "Average"){
      return Math.floor(Math.random() * (6 - 1 + 1)) + 1;
    }
    
    if(mode === "Strong"){
      return (Math.floor(Math.random() * (4 - 1 + 1)) + 1) + 2;
    }
    
    if(mode === "Maxed"){
      return 6; 
    }
    
    if(mode === "undefined"){
      return 1;
    }
  }
  
  function displayLabels(pokemon, name) 
  {
    if(/(m)/.test(name)){
      console.log('\♂');
      $("#pokemonName").text(name.replace('(m)', ' \♂') + " - ");
      $("#name").html(name.replace('(m)', ' \♂') + " - ");
    }
    if(/(f)/.test(name)){
      $("#pokemonName").text(name.replace('(f)', ' \♀') + " - ");
      $("#name").html(name.replace('(f)', ' \♀') + " - ");
    }
    if(!/(m)|(f)/.test(name)){
      $("#pokemonName").text(name + " - ");
      $("#name").html(name);
    }
    $("#pokedexID").text("#" + pokemon.Pokedex_ID);
    $("#shiny").html(shiny);
    $("#legend").html(legend);
  }

  function displayHP(pokemon)
  {
    var hp = (ivStrength + evStrength)*2 + ivMind + evMind;
    
    $("#hpBar").append("<span class='actual'>"+displayBoxes(0, hp)+"</span>");
    $("#hpBar").append("<span class='potential'>"+displayBoxes(hp, 36)+"</span>");
  }

  function displayFP(pokemon)
  {
    var fp = ivMind + evMind;

    $("#fpBar").append("<span class='actual'>"+displayBoxes(0, fp)+"</span>");
    $("#fpBar").append("<span class='potential'>"+displayBoxes(fp, 12)+"</span>");
  }

  function displayBoxes(prev, limit)
  {
    for (var i = prev, str = ""; i < limit; i++) { 
      if (i%6 == 0 ) { str += " "; }
      str += "□"; 
    }
    return str;
  }

  function displayCounters(pokemon, counters) 
  {
    var table = $("<table class='twelve columns'></table>");
    var row = $("<tr></tr>");
    //Add header
    row.append("<th class='two columns'></th>");
    if ( pokemon.Types.length == 0 || pokemon.Types.length == 1 )
    {
      row.append("<th class='ten columns'>"+primaryType+"</th>");
    }
    else if ( pokemon.Types.length == 2 )
    {
      row.append("<th class='five columns'>"+primaryType+"</th>");
      row.append("<th class='five columns'>"+secondaryType+"</th>");
    }
    table.append(row);

    //Add table body
    for ( var key in counters[primaryType]) 
    {
      primaryCounters = primaryType != "None" ? counters[primaryType][key] : [];
      secondaryCounters = secondaryType != "None" ? counters[secondaryType][key] : [];
    
      var row = $("<tr></tr>");
      row.append("<td class='label two columns'>"+key+"</td>");
      if ( pokemon.Types.length == 0 || pokemon.Types.length == 1 )
      {
        row.append("<td class='ten columns'>"+displayCounterIcons(primaryCounters)+"</td>");
      } 
      else if ( pokemon.Types.length == 2 ) 
      {
        row.append("<td class='five columns'>"+displayCounterIcons(primaryCounters)+"</td>");
        row.append("<td class='five columns'>"+displayCounterIcons(secondaryCounters)+"</td>");
      }
      table.append(row);
    }
    
    $("#counters").append(table);
  }

  function displayCounterIcons(counters)
  {
    var str = "";
    $.each(counters, function(id, type) {
      str += "<img class='icon two columns' src='assets/images/type_icons/"+type+".png' alt='"+type+"'/>";
    });
    return str;
  }

  function displayDashRange(pokemon, abilityList)
  { 
    var pAbility = abilityList[pokemon.Ability];
    var dash = ivSpeed + pAbility.Stat_Modifiers.Dash;
    var range = (ivSense+1) + pAbility.Stat_Modifiers.Range;

    $("#dash").html(dash);
    $("#range").html(range);
  }

  function displayAbility(pokemon, abilities)
  {
    $("#ability").append("<span class='label'>"+pokemon.Ability+": </span>");
    $("#ability").append("<span>"+ability["Description"]+"</span>");
  }

  function displayEvolution(pokemon)
  {
    if ( typeof pokemon.Evolution !== "undefined" && typeof pokemon.Evolution.Next_Evolutions !== "undefined" && pokemon.Evolution.Next_Evolutions != null )
    {
      $("#evolution").html(evolutionMethod);
      $("#nextEvolution").html(nextEvolution);
    } else {
      $("#evolution").html("Final Form");
    }
  }

  function displayNature(pokemon, natures)
  {
    var total = 0;
    $.each(natures, function(name, nature) {
      var weight = nature.Weight;
      total += weight;
    });

    var rand = Math.random()*total;
    var runningTotal = 0;
    var key = "";
    $.each(natures, function(name, nature) {
      var weight = nature.Weight;
      runningTotal += weight;
      if ( rand < runningTotal ) { 
        key = name; 
        return false; //jquery break
      }
    });

    $("#nature").append("<span class='header'>"+key.replace(/_/g, " ")+": </span>");
    $("#nature").append("<span>"+natures[key]["Description"]+"</span>");
  }

  function displayPokemon(pokemon, pokemonName)
  {
    var name = null;
    if(/'/.test(pokemonName)){
      name = "Farfetch";
    }
    $("#pokemon").addClass("border").addClass(primaryType.toLowerCase() + "border");
    
    //important to keep the pokemon as a div because of grid changes
    if(name === null){
    $("#pokemon").append("<span class='twelve columns'>" +
      "<h5>"+
      "<span id='gender'></span>"+
      "<span id='eggGroups' class='header ten columns'></span>"+
      "</h5>"+
      "</span>"+
      "<div class='offset-by-four columns'><img class='pokepic five columns' src='assets/images/pokemon/"+pokemonName+".png'></div>"+
      "</span>");
    }

    if(name !== null){
      console.log(name);
      $("#pokemon").append("<span class='twelve columns'>" +
        "<h5>"+
        "<span id='gender'></span>"+
        "<span id='eggGroups' class='header ten columns'></span>"+
        "</h5>"+
        "</span>"+
        "<div class='offset-by-four columns'><img class='pokepic five columns' src='assets/images/pokemon/"+name+".png'></div>"+
        "</span>");
    }
    
    $("#pokemon").tooltip({
        content: "<div><p>"+ pokemon.Long_Description +"</p></div>",
        open: function(event, ui)
            {
              ui.tooltip.hover(
                function () {
                  $(this).fadeTo("slow", 1);
          });
            },
        track: true
      });

    displayGender(pokemon);
    displayEggGroups(pokemon);
  }

  function displayGender(pokemon)
  {
    var rand = Math.random() * (male+female+other);
    if ( rand <= male ) { $("#gender").append("<img class='icon offset-by-one one column' src='assets/images/gender_icons/male.png' alt='male'/>");}
    else if ( rand <= male+female ) { $("#gender").append("<img class='icon offset-by-one one column' src='assets/images/gender_icons/female.png' alt='female'/>");}
    else if ( rand <= male+female+other ) { $("#gender").append("<img class='icon offset-by-one one column' src='assets/images/gender_icons/other.png' alt='other'/>");}
  }

  function displayEggGroups(pokemon)
  {
    if ( typeof pokemon.Egg_Groups !== "undefined" )
    { 
      var eggGroups = pokemon.Egg_Groups;
      if ( eggGroups.length == 1 )
      {
        $("#eggGroups").append("<span class='center twelve columns'>"+ eggGroups[0] +"</span>");
      }
      else if ( eggGroups.length == 2)
      {
        $("#eggGroups").append("<span class='center six columns'>"+ eggGroups[0] +"</span>");
        $("#eggGroups").append("<span class='center six columns'>"+ eggGroups[1] +"</span>"); 
      }
    } 
    else 
    {
       $("#eggGroups").append("<span class='center twelve columns'> None </span>");
    }
    
  }

  function displayAttributes(pokemon) 
  {
    displayAttributeIV($("#iv_speed"), ivSpeed );
    displayAttributeIV($("#iv_strength"), ivStrength );
    displayAttributeIV($("#iv_sense"), ivSense );
    displayAttributeIV($("#iv_mind"), ivMind );

    displayAttributeEV($("#ev_speed"), evSpeed );
    displayAttributeEV($("#ev_strength"), evStrength );
    displayAttributeEV($("#ev_sense"), evSense );
    displayAttributeEV($("#ev_mind"), evMind );
  }

  function displayAttributeEV(element, val)
  {
    for ( var i = 0, str =""; i < 6; i++) { 
      str += i < val ? "●" : "○"; 
    }
    element.html(str);
  }

  function displayAttributeIV(element, val)
  {
    for ( var i = 0, str =""; i < 6; i++) { 
      str += i >= 6-val ? "●" : "○"; 
    }
    element.html(str);
  }

  function displayMoves(pokemon, moves)
  { 
     var pokemonMoves = typeof pokemon.Moves !== "undefined" && typeof pokemon.Moves.Learnable !== "undefined" ? pokemon.Moves.Learnable : [];
     var knownMoves = [];
     var randomMoves = [];
     var availableMoves = [];
     var difficultMoves = 0;
     var missingMoves = 0;
     
      if(mode === "Starter" || mode === "undefined"){
        knownMoves.push(pokemonMoves[0], pokemonMoves[1]);
      }
    
      if(mode === "Weak"){
        knownMoves.push(pokemonMoves[0]);
        randomMoves = generateMoves(pokemonMoves.slice(1), moves);
        knownMoves.push(randomMoves[0]);
      }
      
      if(mode === "Average"){
        randomMoves = generateMoves(pokemonMoves, moves);
        knownMoves = randomMoves.slice(0,4);
      }
      
      if(mode === "Strong"){
        randomMoves = generateMoves(pokemonMoves, moves);
        knownMoves = randomMoves.slice(0,5);
      }
      
      if(mode === "Maxed"){
        randomMoves = generateMoves(pokemonMoves, moves);
        knownMoves = randomMoves.slice(0,6);
      }

      availableMoves = deDupe(knownMoves, pokemonMoves);
     
     var accordion = $("<div id='accordion' class='twelve columns'></div>");
     $.each(knownMoves, function(id, pokemonMove) 
     {
        var move = findMove(moves, pokemonMove);
        if (move != null)
        {
          var moveName = pokemonMove.replace(/ /g, '_');
          var flavor = typeof move.flavor !== "undefined" ? move.flavor : "No text!";
          var power = typeof move.power !== "undefined" ? move.power : "0";
          var style = typeof move.style !== "undefined" ? move.style : "Any";
          
          if(move.difficulty === "Difficult" || move.difficulty === "Impossible"){
            difficultMoves += 1;
          }

          //Create header div for accordion
          var container = $("<article class='group twelve columns'></div>");

          var header = $("<div class='move-header twelve columns "+ move.type.toLowerCase() +"' id='header_"+moveName+"'></div>");
          header.append("<span class='one-half column'><img class='icon' src= 'assets/images/type_icons/"+move.type+".png' /></span>");
          header.append("<span class='three columns'><span class='label'>"+moveName.replace(/_/g, ' ')+"</span><span> - "+style+"</span>");
          header.append("<span class='eight columns'>" + flavor +"</span>");
          header.append("<span style='text-align: right;' class='label one-half columns'>" + power + "</span>");
          container.append(header);
         
          //Create content div for accordion
          var content = $("<div class='move-content twelve columns' id='content_"+moveName+"'></div>");
          var effect = typeof move.effect !== "undefined" ? move.effect : "No effect";
          var critical = typeof move.critical !== "undefined" ? move.critical : "";
          var outofbattle = typeof move.out_of_battle !== "undefined" ? move.out_of_battle : "";
          var moveStyle = typeof move.style !== "undefined" ? move.style : "";
          var moveLevel = typeof move.difficulty !== "undefined" ? move.difficulty : "";
          var critLine = "";

          content.append("<p><span class='label'> Effect: </span>" + effect + "</p>");
          if (critical !== "" || moveLevel !== "") { critLine = "<p><span class='label'> Critical: </span>" + critical + "<span class='label'> Difficulty: </span>" + moveLevel +"</p>"; }
          content.append(critLine);
          if (outofbattle != "") { content.append("<p><span class='label'> Out Of Battle: </span>" + outofbattle + "</p>"); }
          container.append(content);

          accordion.append(container);
        } else {
          missingMoves += 1;
          console.error("The following move returned Null: %s", pokemonMove);
        }
      });
      $("#moves").append(accordion);
      accordion
        .accordion({
          header: ".move-header",
          heightStyle: "",
          collapsible: true
        })
        .sortable({
          axis: "y",
          handle: ".move-header",
          stop: function( event, ui ) {
            // IE doesn't register the blur when sorting
            ui.item.children(".move-header").triggerHandler( "focusout" );
            // Refresh accordion to handle new order
            $( this ).accordion( "refresh" );
          }
      });

      // $("#pageBreak").append("<div style="page-break-after: always;"></div>");
      
      // List all learnable moves for the Pokemon
      accordion = $("<div id='accordion' class='twelve columns'></div>");
      $.each(availableMoves, function(id, pokemonMove) 
     {
        var move = findMove(moves, pokemonMove);
        if (move != null)
        {
          var moveName = pokemonMove.replace(/ /g, '_');
          var flavor = typeof move.flavor !== "undefined" ? move.flavor : "No text!";
          var power = typeof move.power !== "undefined" ? move.power : "0";
          var style = typeof move.style !== "undefined" ? move.style : "Any";
          
          //Create header div for accordion
          var container = $("<article class='group twelve columns'></div>");

          var header = $("<div class='move-header twelve columns "+ move.type.toLowerCase() +"' id='header_"+moveName+"'></div>");
          header.append("<span class='one-half column'><img class='icon' src= 'assets/images/type_icons/"+move.type+".png' /></span>");
          header.append("<span class='three columns'><span class='label'>"+moveName.replace(/_/g, ' ')+"</span><span> - "+style+"</span>");
          header.append("<span class='eight columns'>" + flavor +"</span>");
          header.append("<span style='text-align: right;' class='label one-half columns'>" + power + "</span>");
          container.append(header);
         
          //Create content div for accordion
          var content = $("<div class='move-content twelve columns' id='content_"+moveName+"'></div>");
          var effect = typeof move.effect !== "undefined" ? move.effect : "No effect";
          var critical = typeof move.critical !== "undefined" ? move.critical : "";
          var outofbattle = typeof move.out_of_battle !== "undefined" ? move.out_of_battle : "";
          var moveStyle = typeof move.style !== "undefined" ? move.style : "";
          var moveLevel = typeof move.difficulty !== "undefined" ? move.difficulty : "";
          var critLine = "";

          content.append("<p><span class='label'> Effect: </span>" + effect + "</p>");
          //if (moveStyle != "") { content.append("<p><span class='label'> Style: </span>" + moveStyle + "</p>"); }
          if (critical !== "" || moveLevel !== "") { critLine = "<p><span class='label'> Critical: </span>" + critical + "<span class='label'> Difficulty: </span>" + moveLevel +"</p>"; }
          content.append(critLine);
          if (outofbattle != "") { content.append("<p><span class='label'> Out Of Battle: </span>" + outofbattle + "</p>"); }
          // if (moveLevel != "") { content.append("<p><span class='label'> Difficulty: </span>" + moveLevel + "</p>"); }
          container.append(content);

          accordion.append(container);
        }else {
          missingMoves += 1;
          console.error("The following move returned Null: %s", pokemonMove);
        }
      });
      $("#learnableMoves").append(accordion);
      accordion
        .accordion({
          header: ".move-header",
          heightStyle: "",
          active: false,
          collapsible: true
        })
        .sortable({
          axis: "y",
          handle: ".move-header",
          stop: function( event, ui ) {
            // IE doesn't register the blur when sorting
            ui.item.children(".move-header").triggerHandler( "focusout" );
            // Refresh accordion to handle new order
            $( this ).accordion( "refresh" );
          }
      });

      if(difficultMoves > 0){
        alert("This Pokemon knows " + difficultMoves + " difficult or impossible moves.");
      }

      if(missingMoves > 0){
        alert("One or more moves returned Null, press F12 to check the console for a list.");
      }
  }

  function findMove(moves, pokemonMove) 
  {
    var move = null;
    $.each(moves, function(type, moveList) {
      if (moveList[pokemonMove])
      {
        move = moveList[pokemonMove];
        move["type"] = type;
        return false;
      } 
    });
    return move;
  }

  /**
   * This function shuffles the available moves for a Pokemon and
   * returns the list of random moves excluding impossible moves
   * Parameters:
   * available: The list of moves available to the Pokemon
   * all: The entire list of moves from moves.json
   */
  function generateMoves(available, all){
    var specificMoves = [];
    var move = null;
    $.each(available, function(type, pMove){
      move = findMove(all, pMove);
      if(move !== null && move.difficulty !== "Impossible"){
        specificMoves.push(pMove);
      }
    });
    specificMoves = shuffle(specificMoves);
    return specificMoves;
  }
  
  // This function shuffles the entire set of moves for a Pokemon and returns the result
  function shuffle(o) {
      for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
      return o;
  }

  // This function adds a fake property to the json object to denote difficulty
  function setDifficulty(moveList, pokemon){
    var strTotal = evStrength + ivStrength;
    var mindTotal = evMind + ivMind;
    var stabBonus = false;
    //console.log("Current Stats:\nstrTotal: %s\nmindTotal: %s\nivStrength: %s\nivMind: %s", strTotal, mindTotal, ivStrength, ivMind);
    var movePool = moveList;
    for(var key in movePool){
      if(!movePool.hasOwnProperty(key)){
        continue;
      }

      if(pokemon.Types.includes(key) && key !== "Steel"){
        console.log(key);
        //console.log("STAB Bonus Added");
        stabBonus = true;
        strTotal += 3;
        mindTotal += 3;
        ivStrength += 3;
        ivMind += 3;
      }

      if(key === "Steel" && pokemon.Ability === "Natural_Steel"){
        //console.log("Steel STAB Bonus Added");
        stabBonus = true;
        strTotal += 3;
        mindTotal += 3;
        ivStrength += 3;
        ivMind += 3;
      }

      //console.log("Current Stats:\nstrTotal: %s\nmindTotal: %s\nivStrength: %s\nivMind: %s", strTotal, mindTotal, ivStrength, ivMind);

      var objType = movePool[key];
      for(move in objType){
        var obj = objType[move];
        obj.difficulty = "";
        switch(obj.style){
          case "Melee":
            if((obj.power / 10) > (strTotal)){
              obj.difficulty = "Impossible";
            }
            if((obj.power / 10) <= (strTotal)){
              obj.difficulty = "Difficult";
            }
            if((obj.power / 10) <= ivStrength) {
                obj.difficulty = "Natural";
            }
            break;
          case "Ranged":
            if((obj.power / 10) > (mindTotal)){
              obj.difficulty = "Impossible";
            }
            if((obj.power / 10) <= (mindTotal)){
              obj.difficulty = "Difficult";
            }
            if((obj.power / 10) <= ivMind) {
                obj.difficulty = "Natural";
            }
            break;
          case "Self":
            if((mindTotal) > (strTotal)){
              if((obj.power / 10) > (mindTotal)){
                obj.difficulty = "Impossible";
              }
              if((obj.power / 10) < (mindTotal)){
                obj.difficulty = "Difficult";
              }
              if((obj.power / 10) <= ivMind) {
                obj.difficulty = "Natural";
              }
            } else {
              if((obj.power / 10) > (strTotal)){
                obj.difficulty = "Impossible";
              }
              if((obj.power / 10) < (strTotal)){
                obj.difficulty = "Difficult";
              }
              if((obj.power / 10) <= (ivStrength)) {
                obj.difficulty = "Natural";
              }
            }
            break;
          case "Weather":
            if((mindTotal) < (strTotal)){
              if((obj.power / 10) > (mindTotal)){
                obj.difficulty = "Impossible";
              }
              if((obj.power / 10) < (mindTotal)){
                obj.difficulty = "Difficult";
              }
              if((obj.power / 10) <= (ivMind)) {
                obj.difficulty = "Natural";
              }
            } else {
              if((obj.power / 10) > (strTotal)){
                obj.difficulty = "Impossible";
              }
              if((obj.power / 10) < (strTotal)){
                obj.difficulty = "Difficult";
              } 
              if((obj.power / 10) <= (ivStrength)) {
                obj.difficulty = "Natural";
              }
            }
            break;
        }
      }
      if(stabBonus){
        strTotal -= 3;
        mindTotal -= 3;
        ivStrength -= 3;
        ivMind -= 3;
        stabBonus = false;
        //console.log("Stats Reset");
      }
      //console.log("Current Stats:\nstrTotal: %s\nmindTotal: %s\nivStrength: %s\nivMind: %s", strTotal, mindTotal, ivStrength, ivMind);
    }
    return movePool;
  }

  function deDupe(known, available){
    var reduced = [];
    for(var i = 0; i < available.length; i++){
      if(!known.includes(available[i])){
        reduced.push(available[i]);
      }
    }
    return reduced;
  }

  /**
   * This expands the accordions and initiates the browser print functions
   */
  $('#print').click(function(){
    var answer = confirm ("Do you wish to print this page's contents?");
    if (answer) {
      $('.ui-accordion-content').show();
      $('.ui-state-active').addClass('ui-state-default').removeClass('ui-state-active');
      print(document);
      $('.ui-accordion-content').hide();
      $('.ui-state-default').addClass('.ui-state-active').removeClass('.ui-state-default');
    }
  });
});
